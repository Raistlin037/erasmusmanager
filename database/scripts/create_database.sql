/* Creamos el usuario erasmusmanager, le concedemos privilegios, los refrescamos y creamos la BD.

   Fecha inicio: 11/03/2018
   Autor: Alfredo Oltra
   Modificado: Javier Alcolea 27/03

   Ejecución:
          Desde el directorio 

          mysql -u root -p < ./database/scripts/create_database.sql

          Donde root es el login del administrador de MySQL. 

*/
 CREATE USER 'erasmusmanager'@'localhost' IDENTIFIED BY 'secret';
 GRANT ALL PRIVILEGES ON * . * TO 'erasmusmanager'@'localhost';
 FLUSH PRIVILEGES;
 CREATE DATABASE erasmusmanager;