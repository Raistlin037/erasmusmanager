// Librería de timelines del apartado welcome

export const Tl = {
    welcome: function() {
        var tl = new TimelineMax();
            tl.from("h2", 0.75, {opacity:0})
                .from("h2", 1, {y:"15vh", ease:Back.easeOut});
            TweenMax.from("blockquote", 0.25, {opacity:0, delay:1.15});
            TweenMax.from("blockquote", 1, {y:"15vh", ease:Back.easeOut, delay:1.15});
            TweenMax.from("#welcome_select_container", 0.25, {opacity:0, delay:1.35});
            TweenMax.from("#welcome_select_container", 1, {y:"15vh", ease:Back.easeOut, delay:1.35});
    },
    headerFooter: function() {
        TweenMax.fromTo("header", 1, {y: "-100%", ease:Power3.easeOut}, {y: "0%", opacity: 1});
        TweenMax.fromTo("footer", 1, {y: "100%", ease:Power3.easeOut}, {y: "0%", opacity: 1});
    },
    menu: function(){
        var tl = new TimelineMax();
        tl
            .to("#transition_container", 1, {x: "0%", skewX: 0, ease:Power3.easeInOut})
            .to("#menu_container", 1, {x: "0%", skewX: 0, ease:Power3.easeOut}, "+=.25")
            .from("h2", 0.75, {x: "-15vw", opacity: 0, ease: Bounce.easeOut}, "+=1")
            .from("p", 0.5, {opacity: 0}, "-=.15")
            .from("#contact", 0.75, {x: "35%", y: "35%", opacity: 0, onComplete: Tl.contact}, 3)
            .from("#info", 0.75, {x: "-35%", y: "35%", opacity: 0, onComplete: Tl.info}, 3)
            .from("#flats", 0.75, {x: "15%", y: "-35%", opacity: 0, onComplete: Tl.flats}, 3);
    },
    contact: function() {
        var tl1 = new TimelineMax({repeat:-1});
        tl1
            .to("#ala1", 0.5, {rotation: "-=10", transformOrigin: "top right", ease:Power0.easeInOut}, 0)
            .to("#ala1", 0.5, {rotation: "+=10", transformOrigin: "top right", ease:Power0.easeInOut}, 0.5)
            .to("#ala2", 0.5, {rotation: "+=10", transformOrigin: "top left", ease:Power0.easeOut}, 0)
            .to("#ala2", 0.5, {rotation: "-=10", transformOrigin: "top left", ease:Power0.easeOut}, 0.5);
        var tl2 = new TimelineMax({repeat:-1});
        tl2
            .set(".linea", {opacity:0})
            .staggerTo(".linea", 0.95, {opacity: 1, ease:Power0.easeNOne, stagger:1});
    },
    flats: function() {
        var ventanasTl = new TimelineMax({repeat:-1})
		var ventanas=document.getElementsByClassName('ventana'),ventanasArray=Array.from(ventanas);
        ventanasArray.sort(function() {return 0.4-Math.random()});
        ventanasArray.splice(0, ventanasArray.length/2); 	
        ventanasTl
            .staggerTo(ventanasArray, 2, {fill: "#bbbb28"}, 1)
            .to(ventanasArray, 2, {fill: "#8BB7F0"});
    },
    info: function() {
        var tl = new TimelineMax({repeat:-1});
        tl
            .staggerFrom(".pizarra", 2, {opacity: 0}, 2)
            .from("#pizarra_icon1", 2, {opacity: 0}, 1)
            .from("#pizarra_icon2", 2, {opacity: 0}, 3)
            .from("#pizarra_icon3", 2, {opacity: 0}, 9)
            .from("#pizarra_icon4", 2, {opacity: 0}, 11)
            .to(".pizarra", 2, {opacity: 0})
            .to(["#pizarra_icon1", "#pizarra_icon2", "#pizarra_icon3","#pizarra_icon4"], 2, {opacity: 0}, 13);
    }
}